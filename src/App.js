import React from "react";
import "./App.css";
import RandomNuber from "./RandomNumber/RandomNumber";

class App extends React.Component {
  state = {
    array: [],
    showResult: false,
  };

  generateRandom = () => {
    const array = {...this.state.array};
    const number = [];

    while (number.length < 5) {
      const newNum = Math.floor(Math.random() * (36 - 5)) + 5;

      if (number.includes(newNum)) {
        continue;
      }
      number.push(newNum);
    };

    number.sort(function (a, b) {
      return a - b;
    });

    array.array = number;
    array.showResult = true;
  
    this.setState(state => {
      return array;
    });
  };

  render() {
    return (
      <div className="App">
        <div>
          <button onClick={this.generateRandom}> New Number </button>
        </div>
        {this.state.showResult ? (
          <RandomNuber
            one={this.state.array[0]}
            two={this.state.array[1]}
            three={this.state.array[2]}
            four={this.state.array[3]}
            five={this.state.array[4]}
          />
        ) : null}
      </div>
    );
  }
}

export default App;
